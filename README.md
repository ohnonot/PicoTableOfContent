# PicoTableOfContent

**Generates a table of contents for the current page.**

## Installation

Navigate to your PicoCMS plugins folder and clone the repo:

~~~
$> cd plugins
$> git clone https://framagit.org/ohnonot/PicoTableOfContent
$> # or
$> git clone https://notabug.org/ohnonot/PicoTableOfContent
~~~

The plugin is enabled by default, but you need to add `{{ TableOfContent }}` to a twig template, for example:

~~~
...
<div class=content>{{ TableOfContent }}{{ content }}</div>
...
~~~

That is where the TOC will show up.

## Configuration

An example `picotableofcontent.yml` configuration file is supplied. It is well commented.
Copy to your PicoCMS' `config` folder and make adjustments.

## Styling with CSS

By default, it will look for  
`{{ assets_url }}/PicoTableOfContent.css` and add a `<link>` to it,
or else default to  
`{{ plugins_url }}/PicoTableOfContent/PicoTableOfContent.css`.
That way you can provide different styling without touching this repository.

In the page's meta header, you can specify a different file path with `toc_css`. 
The path must be absolute (starting with a `/`), 
and will be applied as `{{ base_url }}{{ meta.toc_css }}` without further checks. E.g.:

~~~
toc_css: /themes/sometheme/css/different.css
# or
toc_css: /assets/css/different.css
# etc.
~~~

## Selectively disable TOC through meta header

Add this to the page's meta header if you don't want a TOC for that page:

~~~
toc_disabled: true
~~~

## Header IDs and links

The ToC generates `id`'s from the actual header text:

```
$id = preg_replace('~[^\\pL0-9_]+~u', '-', $header_text);
$id = trim($id,'-');
$id = strtolower($id);
```
It replaces "weird" characters and whitespace with `-`, collapses subsequent `-` into one, and makes the resulting string all lowercase.

A header like "This Works (for me)" would become `this-works-for-me`.
